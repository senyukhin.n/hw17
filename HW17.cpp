﻿// HW17.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;
public:
    Vector()
    {}
    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    double l = sqrt(x * x + y * y + z * z);
    void Show()
    {
        std::cout << x << ' ' << y << ' ' << z << '\n'<< l;
    }
};
int main()
{
    Vector v(0,6,8);
    v.Show();
}

